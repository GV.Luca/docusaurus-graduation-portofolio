---
id: glossary
title: Glossary
sidebar_label: Glossary
slug: /glossary
---
Following is a list of commonly used terms throughout the portfolio.

### [Ansible](https://www.ansible.com/)
Ansible is an open-source software provisioning, configuration management, and application-deployment tool enabling infrastructure as code.


### API (Application Programming Interface) 

In computing, an application programming interface is an interface that defines interactions between multiple software applications or mixed hardware-software intermediaries.

### Alert

Something that requires human attention. True positive **alerts** mean that something suspicious is going on in your organization, requiring action. False-positive alerts mean that you’ll need to optimize your detection set up in order to lower its noise. **Alerts** is basically a **high-risk state** that is added to one or more events.

### Alerting systems

An alerting system is a platform you can use to centralize alerts from various tools and systems and distribute those alerts to professionals, who can remedy the incident or the wider business ecosystem that need to be informed.


### [AWS (Amazon Web Services)](https://aws.amazon.com/)

Amazon Web Services is a subsidiary of Amazon providing on-demand cloud computing platforms and APIs


### [Azure AD (Active Directory)](https://azure.microsoft.com/en-us/services/active-directory/)

Azure Active Directory (Azure AD) is Microsoft's enterprise cloud-based identity and access management (IAM) solution.


### Azure administrator

User with the role of managing the instances of the cloud infrastructure services and the multiple cloud servers.

### [Azure Cloud](https://azure.microsoft.com/en-us/)

Microsoft Azure commonly referred to as Azure, is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and services through Microsoft-managed data centers.

### [Azure Roles](https://docs.microsoft.com/en-us/azure/role-based-access-control/built-in-roles)

Azure role-based access control (Azure RBAC) has several Azure built-in roles that you can assign to users, groups, service principals, and managed identities. Role assignments are the way you control access to Azure resources.


### Brick

An event-driven processor with one specific function, varying from collecting a Twitter feed, enriching data with machine learning, to storing data inside a specific database.

### [Chef](https://www.chef.io/)

Chef is a company and the name of a configuration management tool written in Ruby and Erlang. It uses a pure-Ruby, domain-specific language (DSL) for writing system configuration "recipes".

### CI/CD (Continous Integration, Continous Development)
In software engineering, CI/CD or CICD is the combined practice of continuous integration and either continuous delivery or continuous deployment.

### Cloud computing

Cloud computing is the on-demand availability of computer system resources, especially data storage and computing power, without direct active management by the user.

### Credentials

Passwords and tokens to request authorization for performing operations with the assigned privileges by the user who created them.

### Datacenters

A data center is a dedicated building, space or a group of spaces/buildings that are used to house computer systems, more often than not data.

### Data Retention

Policy for what happens with data. A **Data Retention Policy** describes rules about how to store data, how to remove data, who is responsible, and how long the data is stored.

### Debian GNU/Linux or Debian

Debian, also known as Debian GNU/Linux, is a Linux distribution composed of free and open-source software, developed by the community-supported Debian Project, which was established by Ian Murdock on August 16, 1993


### [Elasticsearch](https://www.elastic.co/)

Elasticsearch is a search engine based on the Lucene library. It provides a distributed, multitenant-capable full-text search engine with an HTTP web interface and schema-free JSON documents.

### Flow

A data pipeline inside **Raven**, defined by connecting several functional bricks together.

### Geolocation

Geopositioning, also known as tracking, geolocalization, geolocating, geolocation, or geoposition fixing is the process of determining or estimating the geographic position of an object

### [Hashicorp](https://www.hashicorp.com/)

HashiCorp is a software company with a Freemium business model based in San Francisco, California. HashiCorp provides open-source tools and commercial products that enable developers, operators, and security professionals to provision, secure, run and connect cloud-computing infrastructure.

### [HoneyTrap](https://docs.honeytrap.io/)
HoneyTrap is a DTACT specific Honeypot solution, a machine that is intentionally vulnerable and the public that is used to lure malicious elements to run their scripts and attacks. The reason for this is to use the data of the attacks and to discover the IPs, locations, and information of the attackers.

### IaaS (Infrastructure as a Service)

Infrastructure as a service are online services that provide high-level APIs used to dereference various low-level details of underlying network infrastructure like physical computing resources, location, data partitioning, scaling, security, backup


### [IAC (Infrastructure as Code)](../portofolio/iac)

Infrastructure as code is the process of managing and provisioning computer data centers through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools.

### IP (Internet Protocol)

An Internet Protocol address is a numerical label assigned to each device connected to a computer network that uses the Internet Protocol for communication. An IP address serves two main functions: host or network interface identification and location addressing.


### Modular platform.

A modular platform or application is an application that has multiple interchangeable components that can be used to create custom build solutions or use cases. 

### Observable

Something that has been **observed** during a certain event or that is derived from an event through correlation with other data sources (for example, a WHOIS database). In cybersecurity, **observables** are typically things like IP addresses, hostnames, file hashes, etc.

### Orchestration

In system administration, orchestration is the automated configuration, coordination, and management of computer systems and software. E.G. Ansible, Puppet, Salt, Terraform, and AWS CloudFormation.

### [Packer](https://www.packer.io/)

Packer is an open-source tool for creating identical machine images for multiple platforms from a single source configuration.

### POC (Proof Of Concept)

Proof of concept, also known as proof of principle, is a realization of a certain method or idea in order to demonstrate its feasibility.

### Provisioing

The process of preparing and equipping a network to allow it to provide new services to its users.


### [RBAC (Role-based access control)](https://docs.microsoft.com/en-us/azure/role-based-access-control/overview)

Role-based access control (RBAC) is a policy-neutral access-control mechanism defined around roles and privileges. 

### Risk Assessment (change from analysis to assessment)

A Security Risk Assessment (or SRA) is an assessment that involves identifying the risks in your company, your technology, and your processes to verify that controls are in place to safeguard against security threats

### SCRUM

Scrum is a framework utilizing an agile mindset for developing, delivering, and sustaining complex products, with an initial emphasis on software development and software integration.

### [Sharepoint](https://www.microsoft.com/en-ww/microsoft-365/sharepoint/collaboration)

SharePoint is a web-based collaborative platform that integrates with Microsoft Office.

### SOC (Security operations center)

A security operations center is a centralized unit that deals with security issues on an organizational and technical level.

### SSH (Secure Shell Protocol)
The Secure Shell Protocol is a cryptographic network protocol for operating network services securely over an unsecured network.

### Stremlining

Make a system more efficient and effective by employing faster or simpler working methods

### Source

The base the data is coming from.

### [Terraform](https://www.terraform.io/intro/index.html)

Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied.

### Token

Tokens are the core method for authentication within Vault and other token-based security systems.

### Trello

Trello is a web-based, Kanban-style, list-making application and is a subsidiary of Atlassian

### Trigger

Action when a certain condition is met.

### [VirtualBox](https://www.virtualbox.org/)

Oracle VM VirtualBox is a free and open-source hosted hypervisor for x86 virtualization, developed by Oracle Corporation.

### VPN (Virtual Private Network)

A virtual private network extends a private network across a public network and enables users to send and receive data across shared or public networks as if their computing devices were directly connected to the private network.

### [WireGuard](https://www.wireguard.com/)
WireGuard is a communication protocol and free and open-source software that implements encrypted virtual private networks.