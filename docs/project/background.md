---
id: background
title: Project Background
sidebar_label: Background
slug: /background
---

DTACT has originally started as **DutchSec** a computer and Network Security company, its
products have evolved from cloud network analysis and monitoring to full data enrichment and
flow building for companies.

Their main products are **Raven** and **HoneyTrap**, **data analytics** and  **data collecting tools**.
Because of the sensitive nature of the data that is being processed, there are multiple layers of
**security** that are taken into consideration. The first one being **infrastructure**.

The Raven product has been developed for, and in the **AWS cloud environment**, for that reason security measures had to be taken
to ensure that the environment that Raven runs on is secure and that the environment **can be replicated** for clients.

To better understand how AWS is used a minimum understanding of **IaaS** (Infrastructure as a Service) is needed. With the emergence of cloud computing so came the ability to sell computing, storage, and networking power. This means that instead of buying a server you **rent** one **from a cloud provider**
and pay as you use it. **Like any infrastructure** physical or digital, it has **networking components**, **different levels of access**,
**monitoring**, and **alerting**.

While cloud platforms make a great job at streamlining the process of creating infrastructure, **managing a company infrastructure** and **improving it**
is an increasingly **hard** job as a company grows. To add to that, the more the infrastructure of a
company **develops** the **more insecure** it might become. To solve this problem a new infrastructure management paradigm was chosen,
[IAC](../portofolio/iac) (Infrastructure as Code).

For their current development and production environment, they have used provisioning through an IAC tool. Now, as they are
looking to expand to different cloud platforms, they want to use the same method to ensure **security and ease of management**.

This means **recreating** their current infrastructure in another cloud environment, **automating** it through their current CI/CD
pipeline (Continuous Integration, Continuous Development) and **safely storing** the credentials needed for the entire process.

While DTACT has worked with provisioning tools before it has never done any research into using its product with **Azure**. As such,
part of the research is finding ways of securely deploying their data analytics tool Raven, through a **new**, yet similar
environment using the same methods used for the AWS environment.