---
id: strategy
title: Strategy
sidebar_label: Strategy
slug: /strategy
---

## Goal
------------------------------------------------

The goal of this project is to **research and create a proof of concept** fully provisioned environment in the Azure cloud that
**contains** all the **security measures** needed to function as an **autonomous infrastructure**.
It is expected that by the end of the project the company would have an **Azure account** that has fully automated
and **provisioned infrastructure** with user groups, VM (virtual machines) security groups, and credential protection.


For further details of the project goal, please refer to the Goal chapter of the [Project Plan](../../static/documents/ImprovedProjectPlan.pdf). 



## Phasing and Strategy
------------------------------------------------

To achieve the project goal, the activities of the project have been broken down into achievable subparts:
1.	Initial Phase:   Determine the Goal and Plan of the project
2.	Implementation Phase:
    
    •	Sprint based **Research**: research the Azure environment and how to best secure and provision it

    •	Sprint based **Development**: implement the Azure-based environment prototype. 
3.	Presentation:  Pass knowledge from findings and experimenting to DTACT.

![img](../../static/img/Phaseing.png)
Figure 1 Phasing Diagram

-------------------------------------------------


### [Project Plan](../../static/documents/ImprovedProjectPlan.pdf) 

---------------------------------------------