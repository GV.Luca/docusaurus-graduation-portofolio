module.exports = {
  title: 'Secure Cloud Automated Provisioning and Security Management',
  tagline: 'Gabriel Vlad Luca',
  url: 'https://docusaurus-graduation-portofolio.netlify.com',
  baseUrl: '/',
  onBrokenLinks: 'ignore',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'GabrielVladPortofolio', // Usually your GitHub org/user name.
  projectName: 'docusaurus-graduation-portofolio', // Usually your repo name.
  noIndex: true, // Don't allow indexing of this site
  themeConfig: {
    navbar: {
      title: 'Graduation Project',
      logo: {
        alt: 'DTACT Logo',
        src: 'img/dLogo.svg',
      },
      hideOnScroll: true,
      items: [
        {
          to: 'portofolio/',
          activeBasePath: 'portofolio',
          label: 'Portofolio',
          position: 'left',
        },
        {
          href: 'https://github.com/Vladlgv/docusaurus-graduation-portofolio',
          label: 'GitHub',
          position: 'right',
        },
        {
          href: 'https://dtact.com',
          label: 'dtact.com',
          position: 'right',
        },
      ],
    },
    // Search:
    algolia: {
      apiKey: '52e48ca4e67bdf532a8d18848c935aa7',
      indexName: 'dtact-docs',
      contextualSearch: false, // Optional: see doc section bellow
      searchParameters: {}, // Optional: Algolia search parameters
    },
    // Google tag
    gtag: {
      trackingID: 'G-KN3ZGSV0VP',
      // Optional fields.
      anonymizeIP: true, // Should IPs be anonymized?
    },
    footer: {
      logo: {
        alt: 'DTACT',
        src: 'img/dLogo.svg',
        href: '/',
      },
      copyright: `Copyright © ${new Date().getFullYear()} Gabriel Vlad Luca`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/portofolio/'
        },
        // blog: {
        //   showReadingTime: true,
        // },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
