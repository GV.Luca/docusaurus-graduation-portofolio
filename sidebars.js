module.exports = {
  someSidebar: {
     "Introduction": ['start/howto','start/intro', 'start/company'],
     "Portofolio Path": ['project/background','project/strategy', 'project/initial', 'project/ravenpage' , 'project/sprint1', 'project/sprint2', 'project/sprint3', 'project/terraform', 'project/sprint4', 'project/conclusion'],
    "Additional resources": ['helpers/glossary', 'helpers/appendix', 'helpers/iac', 'helpers/manage','helpers/git']

  },
};
